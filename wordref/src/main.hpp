/*
WordReferenceCLI, a c++ program to access WordReference.com.
Copyright (C) 2013  King_DuckZ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id80FAB3D5977B4A1797F2910935B34AEA
#define id80FAB3D5977B4A1797F2910935B34AEA

#include <string>
#include <map>
#include <stdexcept>
#include <algorithm>
#include <memory>
#include <ostream>

#define pure_function __attribute__((pure))

#endif
