#ifndef idC90956E8210D439AA5E2801D9E9FD799
#define idC90956E8210D439AA5E2801D9E9FD799

class HttpReader {
public:
	HttpReader ( void );
	~HttpReader ( void ) noexcept;

	std::string GetPage ( const std::string& parAddress ) const;
	std::wstring GetPageW ( const std::string& parAddress ) const;
	std::string UrlEncode ( const wchar_t* parWord ) __attribute__((pure));
	std::string UrlEncode ( const std::wstring& parWord ) __attribute__((pure));
	std::wstring UrlEncodeW ( const wchar_t* parWord ) __attribute__((pure));
	std::wstring UrlEncodeW ( const std::wstring& parWord ) __attribute__((pure));

private:
	struct LocalData;

	const std::unique_ptr<LocalData> m_localData;
};

#endif
