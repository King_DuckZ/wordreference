/*
WordReferenceCLI, a c++ program to access WordReference.com.
Copyright (C) 2013  King_DuckZ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.hpp"
#include "HttpReader.hpp"
#include "CharConv.hpp"
#include <curl/curl.h>
#include <sstream>
#include <boost/algorithm/string/trim_all.hpp>

namespace {
	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	extern "C" int readCurlHelper(char* parData, size_t parSize, size_t parNmemb, void* parBuffer) {
		std::string* const that = static_cast<std::string*>(parBuffer);
		const size_t dataLen = parSize * parNmemb;
		that->reserve(that->size() + dataLen);
		const size_t effectiveWrite = std::min(dataLen, that->capacity() - that->size());
		that->append(parData, effectiveWrite);
		return effectiveWrite;
	}

	std::string GetCleanWord (CURL* parCurl, std::wstring parWord) __attribute__((pure));

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::string GetCleanWord (CURL* parCurl, std::wstring parWord) {
		class CurlFree {
		public:
			CurlFree ( void ) noexcept = default;
			void operator() ( char* parDele ) const { curl_free(parDele); }
		};

		boost::algorithm::trim_all(parWord);
		const std::string wordMultibyte(cconv::WideToMultibyte(parWord));
		const std::unique_ptr<char, CurlFree> urlEncoded(curl_easy_escape(parCurl, wordMultibyte.c_str(), static_cast<int>(wordMultibyte.size())));
		std::string retVal(urlEncoded.get());
		return retVal;
	}
} //unnamed namespace

struct HttpReader::LocalData {
	explicit LocalData ( CURL* parCurl ) : curl(parCurl), headers(nullptr) {}

	CURL* const curl;
	struct curl_slist* headers;
};

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
HttpReader::HttpReader() :
	m_localData(new LocalData(curl_easy_init()))
{
	if (m_localData->curl) {
		curl_slist_append(m_localData->headers, "Accept: application/json");
		curl_slist_append(m_localData->headers, "Content-Type: application/json");
		curl_slist_append(m_localData->headers, "charset: utf-8");
	}
	else {
		throw std::runtime_error("Can't create the CURL object, curl_easy_init() failed");
	}
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
HttpReader::~HttpReader() noexcept {
	if (m_localData->curl)
		curl_easy_cleanup(m_localData->curl);
	curl_slist_free_all(m_localData->headers);
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::string HttpReader::GetPage (const std::string& parAddress) const {
	std::string localString;

	curl_easy_setopt(m_localData->curl, CURLOPT_FOLLOWLOCATION, 1L); //Tell curl to follow redirection when needed
	curl_easy_setopt(m_localData->curl, CURLOPT_HTTPHEADER, m_localData->headers);
	curl_easy_setopt(m_localData->curl, CURLOPT_URL, parAddress.c_str());
	curl_easy_setopt(m_localData->curl, CURLOPT_HTTPGET, 1);
	curl_easy_setopt(m_localData->curl, CURLOPT_WRITEFUNCTION, &readCurlHelper);
	curl_easy_setopt(m_localData->curl, CURLOPT_WRITEDATA, &localString);

	const CURLcode res = curl_easy_perform(m_localData->curl);
	if (CURLE_OK != res) {
		std::ostringstream oss;
		oss << "Error during the CURL request: " << curl_easy_strerror(res);
		throw std::runtime_error(oss.str());
	}
	return localString;
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::wstring HttpReader::GetPageW (const std::string& parAddress) const {
	return cconv::MultibyteToWide(this->GetPage(parAddress));
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::string HttpReader::UrlEncode (const wchar_t* parWord) {
	return GetCleanWord(m_localData->curl, parWord);
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::string HttpReader::UrlEncode (const std::wstring& parWord) {
	return GetCleanWord(m_localData->curl, parWord);
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::wstring HttpReader::UrlEncodeW (const wchar_t* parWord) {
	return cconv::MultibyteToWide(this->UrlEncode(parWord));
}

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
std::wstring HttpReader::UrlEncodeW (const std::wstring& parWord) {
	return cconv::MultibyteToWide(this->UrlEncode(parWord));
}
