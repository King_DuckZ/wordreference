/*
WordReferenceCLI, a c++ program to access WordReference.com.
Copyright (C) 2013  King_DuckZ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id75A4E59ADE4649F7A4A56F91C5886405
#define id75A4E59ADE4649F7A4A56F91C5886405

class HttpReader;

enum WordReferenceLangDirection {
	WordRefLangFrom,
	WordRefLangTo
};

void GetAvailableLanguages ( std::vector<const std::string*>& parCodes, std::vector<const std::string*>& parNames );

class ErrBadLanguage : public std::runtime_error {
public:
	ErrBadLanguage ( std::string&& parMessage );
	~ErrBadLanguage ( void ) noexcept = default;
};

class WordReference {
public:
	WordReference ( void ) = delete;
	WordReference ( const std::string& parFrom, const std::string& parTo, const char* parApiKey );
	~WordReference ( void );

	const std::string& GetLanguageCode ( WordReferenceLangDirection parDir ) const;
	const std::string& GetLanguageName ( WordReferenceLangDirection parDir ) const;
	static std::string GetApiVersion ( void );
	std::string GetHttpLink ( const wchar_t* parWord );
	std::string GetHttpLink ( const std::wstring& parWord );
	std::wstring GetHttpLinkW ( const wchar_t* parWord );
	std::wstring GetHttpLinkW ( const std::wstring& parWord );
	void Translate ( const std::wstring& parWord, std::wostream& parStream );

private:
	const std::unique_ptr<HttpReader> m_httpReader;
	std::string m_langFrom;
	std::string m_langTo;
	std::string m_apiKey;
};

#endif
