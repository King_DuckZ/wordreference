#ifndef idA800F41881384E6980F1D79AFCCA0338
#define idA800F41881384E6980F1D79AFCCA0338

namespace cconv {
	class ErrCantConvert : public std::domain_error {
	public:
		ErrCantConvert ( const std::string& parFrom, const std::string& parTo );
		ErrCantConvert ( const char* parFrom, const char* parTo );
		~ErrCantConvert ( void ) noexcept = default;
	};

	std::wstring MultibyteToWide ( const std::string& parMultiByte ) pure_function;
	std::wstring MultibyteToWide ( const char* parMultiByte ) pure_function;
	std::wstring MultibyteToWide ( const char* parMultiByte, size_t parLen ) pure_function;

	std::string WideToMultibyte ( const std::wstring& parWide ) pure_function;
	std::string WideToMultibyte ( const wchar_t* parWide ) pure_function;
	std::string WideToMultibyte ( const wchar_t* parWide, size_t parLen ) pure_function;
} //namespace cconv

#endif
