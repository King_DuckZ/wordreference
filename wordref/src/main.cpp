/*
WordReferenceCLI, a c++ program to access WordReference.com.
Copyright (C) 2013  King_DuckZ

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "main.hpp"
#include "WordReference.hpp"
#include "CharConv.hpp"
#include "cmake_gen.h"
#include <iostream>
#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <sstream>
#include <cstring>

namespace {
	const char* DefApiKey = "58327";

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	const char* GetBaseName (const char* parPath) {
		const size_t len = std::strlen(parPath);
		const char* found;
		const char* currSearch = parPath;
		do {
			found = currSearch;
			currSearch = std::find(found, parPath + len, '/');
		} while (currSearch++ != parPath + len);

		return found;
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	void ShowVersion() {
		std::cout << APP_NAME << " v" << APP_VER_MAJOR << "." << APP_VER_MINOR << "." << APP_VER_REVISION << "\n";
		std::cout << "rev: " << APP_VER_GIT << "\n";
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	void ShowHelp (const boost::program_options::options_description& parDesc) {
		std::cout << parDesc << "\n";
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	bool GetCommandLine (boost::program_options::variables_map& parVarMap, int parArgc, const char* const parArgv[], const std::vector<const char*>& parOverrideArgs) {
		const char* const programName = parArgv[0];
		std::ostringstream oss;
		oss << "Usage is " << GetBaseName(parArgv[0]) << " [options] <from language> <to language> <search term>; parameters";
		boost::program_options::options_description desc(oss.str());
		boost::program_options::positional_options_description positionals;
		positionals.add("source-lang", 1);
		positionals.add("dest-lang", 1);
		positionals.add("word", -1);

		boost::program_options::options_description hidden("hidden options");
		hidden.add_options()
			("source-lang", boost::program_options::value<std::string>(), "source language")
			("dest-lang", boost::program_options::value<std::string>(), "dest language")
			("word", boost::program_options::value<std::string>(), "search term")
		;

		desc.add_options()
			("help,h", "show this help screen")
			("listlanguages,l", "list available languages with their codes")
			("version,v", "show the version of th program and exit")
		;

		boost::program_options::options_description commandLine;
		commandLine.add(desc).add(hidden);
		boost::program_options::store(boost::program_options::command_line_parser(parArgc, parArgv).options(commandLine).positional(positionals).run(), parVarMap);
		boost::program_options::notify(parVarMap);
		bool shownSomething = false;

		const bool enoughParams = static_cast<bool>(
				parVarMap.count("source-lang") == 1 and
				parVarMap.count("dest-lang") == 1 and
				parVarMap.count("word") > 0
		);
		bool ignoreFewerParams = false;
		if (parVarMap.count("help")) {
			ShowHelp(desc);
			shownSomething = true;
		}
		else if (parVarMap.count("version")) {
			ShowVersion();
			shownSomething = true;
		}
		else {
			for (std::vector<const char*>::const_iterator itCur = parOverrideArgs.begin(), itCurEND = parOverrideArgs.end(); itCur != itCurEND; ++itCur) {
				if (parVarMap.count(*itCur)) {
					ignoreFewerParams = true;
					break;
				}
			}
		}

		if (not (shownSomething or ignoreFewerParams or enoughParams)) {
			ShowHelp(desc);
			shownSomething = true;
		}
		return shownSomething;
	}
} //unnamed namespace

///----------------------------------------------------------------------------
///----------------------------------------------------------------------------
int main (int parArgc, const char* const parArgv[]) {
	//std::setlocale(LC_CTYPE, "UTF-8");
	std::setlocale(LC_CTYPE, "");
	boost::program_options::variables_map vm;
	const std::vector<const char*> paramsSkipCompulsoryInput = {"listlanguages"};
	if (GetCommandLine(vm, parArgc, parArgv, paramsSkipCompulsoryInput))
		return 0;

	if (vm.count("listlanguages")) {
		std::vector<const std::string*> codes, names;
		GetAvailableLanguages(codes, names);
		std::cout << "Supported languages are:\n";
		for (size_t z = 0; z < codes.size(); ++z) {
			std::cout << "\t" << *names[z] << " [" << *codes[z] << "]\n";
		}
		std::cout << "\n";
		std::cout << "Note that the current WordReference specifications (Aug 2013) says:\n\"";
		std::cout << "Only to and from English. We do not currently offer any combination that\ndoes not include English (such as Spanish-French).\"\n";
		std::cout << "This program does not enforce this rule, but it may malfunction if you\nchoose the wrong languages.\n";
		return 0;
	}

	{
		const std::string langFrom(vm["source-lang"].as<std::string>());
		const std::string langTo(vm["dest-lang"].as<std::string>());
		const std::wstring searchWord(cconv::MultibyteToWide(vm["word"].as<std::string>()));

		WordReference wref(langFrom, langTo, DefApiKey);
		wref.Translate(searchWord, std::wcout);

		std::wcout << L"\n" << wref.GetHttpLinkW(searchWord) << L"\n";
	}
	std::wcout << L"Written by King_DuckZ; © WordReference.com" << std::endl;
	return 0;
}
