#include "main.hpp"
#include "CharConv.hpp"
#include <cstdlib>
#include <cwchar>
#include <cstring>
#include <iconv.h>
#include <clocale>

namespace cconv {
	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	ErrCantConvert::ErrCantConvert (const std::string& parFrom, const std::string& parTo) :
		std::domain_error(std::string("Can't convert from \"") + parFrom + "\" to \"" + parTo + "\"")
	{
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	ErrCantConvert::ErrCantConvert (const char* parFrom, const char* parTo) :
		std::domain_error(std::string("Can't convert from \"") + parFrom + "\" to \"" + parTo + "\"")
	{
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::wstring MultibyteToWide (const std::string& parMultiByte) {
		return MultibyteToWide(parMultiByte.c_str(), parMultiByte.size());
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::wstring MultibyteToWide (const char* parMultiByte) {
		return MultibyteToWide(parMultiByte, std::strlen(parMultiByte));
	}

	///------------------------------------------------------------------------
	///See: http://www.lemoda.net/c/iconv-example/iconv-example.html
	///------------------------------------------------------------------------
	std::wstring MultibyteToWide (const char* parMultiByte, size_t parLen) {
		const size_t maxCount = parLen + 1;

		//const std::unique_ptr<char[]> iconvMem(new char[maxCount * sizeof(wchar_t)]);
		//{
		//	const char* const localeFrom = "UTF-8"; //std::setlocale(LC_CTYPE, nullptr);
		//	static const char* const localeTo = "WCHAR_T";
		//	const iconv_t iconvHandle = iconv_open(localeFrom, localeTo);
		//	if (reinterpret_cast<iconv_t>(-1) == iconvHandle) {
		//		if (EINVAL == errno)
		//			throw ErrCantConvert(localeFrom, localeTo);
		//		else
		//			std::runtime_error("Error initializing iconv");
		//	}

		//	const std::unique_ptr<char[]> multibyteCopy(new char[parLen + 1]);
		//	std::copy(parMultiByte, parMultiByte + parLen + 1,  multibyteCopy.get());
		//	size_t srcLen = parLen;
		//	size_t dstLen = maxCount;
		//	char* dstMem = iconvMem.get();
		//	char* srcMem = multibyteCopy.get();
		//	const size_t iconvRetVal = iconv(iconvHandle, &srcMem, &srcLen, &dstMem, &dstLen);

		//	const int closeRetVal = iconv_close(iconvHandle);
		//	if (0 != closeRetVal)
		//		throw std::runtime_error("iconv_close() failed");
		//}
		const std::unique_ptr<wchar_t[]> buff(new wchar_t[maxCount]);
		const size_t charCount = std::mbstowcs(buff.get(), parMultiByte, maxCount);
		if (charCount == maxCount)
			buff[maxCount - 1] = L'\0';
		else if (static_cast<size_t>(-1) == charCount)
			throw std::runtime_error("Can't convert received string, mbstowcs() returned an error");
		return std::wstring(buff.get());
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::string WideToMultibyte (const std::wstring& parWide) {
		return WideToMultibyte(parWide.c_str(), parWide.size());
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::string WideToMultibyte (const wchar_t* parWide) {
		return WideToMultibyte(parWide, std::wcslen(parWide));
	}

	///------------------------------------------------------------------------
	///------------------------------------------------------------------------
	std::string WideToMultibyte (const wchar_t* parWide, size_t parLen) {
		const size_t maxMultibSize = parLen * 4 + 1;
		const std::unique_ptr<char[]> memForMultib(new char[maxMultibSize]);

		const size_t wctombsRet = std::wcstombs(memForMultib.get(), parWide, maxMultibSize);
		if (EILSEQ == wctombsRet or static_cast<size_t>(-1) == wctombsRet)
			throw std::runtime_error("Can't convert received string to multibyte, wcstombs() failed");
		return std::string(memForMultib.get(), wctombsRet);
	}
} //namespace cconv
