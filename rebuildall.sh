#!/usr/bin/env bash

function cd {
    if (("$#" > 0)); then
        if [ "$1" == "-" ]; then
            popd &> /dev/null
            return $?
        else
            pushd "$@" &> /dev/null
            return $?
        fi
    else
        cd $HOME
        return $?
    fi
}

for z in Debug Release Final
do
    cd $z
	retCode=$?
	if [ $retCode != 0 ]; then
		mkdir $z && cd $z
		retCode=$?
	fi

    if [ $retCode == 0 ]; then
        rm -rf *
        cmake ../../ -DCMAKE_BUILD_TYPE=$z
        make -j2
		cd ..
    fi
done
